```
    ├── bin
    │   ├── app.php
    │   └── page.php
    ├── deploy
    │   ├── recipe
    │       ├── function
    │           ├── rsync.php
    │           ├── select_branch.php
    │           ├── set_param.php
    │           └── validate.php
    │       └── keywordtool.php
    │   ├── config.php
    │   ├── deploy.php
    │   └── deployer.phar
    ├── public
    │   ├── custom
    │   ├── .htaccess
    │   ├── favicon.ico
    │   ├── index.dev.php
    │   ├── index.live.php
    │   └── index.test.php
    ├── src
    │   ├── Exception
    │   ├── Form
    │   ├── Helpers
    │   ├── Interceptor
    │       └── Annotation
    │   ├── Model
    │       ├── Handler
    │       ├── Injectors
    │       ├── Manager
    │       └── Repositories
    │   ├── Module
    │   ├── Provider
    │   └── Resource
    │       ├── App
    │       └── Page
    ├── tests
    │   ├── Resouce
    │       └── Page
    ├── var
    │   ├── conf
    │   ├── log
    │   ├── templates
    │       ├── elements
    │       ├── error
    │       ├── layouts
    │       └── pages
    │   └── tmp
    ├── vendor
    ├── env.php
    ├── autoload.php
    ├── bootstrap.php
    ├── composer.json
    ├── composer.lock
    └── package.json
```

###### <span style="color:#e6004c;">bin</span>

This is where framework rewires required components and functionalities to make functional.

###### <span style="color:#e6004c;">deploy</span>

Deploy recipe and functionalities for the project.

###### <span style="color:#e6004c;">public</span>

Exposed folders to server. Contains custom css, js, and images files.

###### <span style="color:#e6004c;">src</span>

- <b>Exception</b> -
    Contains exceptions thrown in the application.
<br><br>
- <b>Forms</b> -
    Contains forms rendered in pages using Aura Form builder.
<br><br>
- <b>Interceptor</b> -
    Function that runs before a page is rendered. This is like a convenient mechanism for filtering HTTP request entering your application.
    <br><br>
    * <b>Annotation</b> -
    Annotations are meta-data that can be embedded in source code. Let you inject behavior and can promote decoupling.
<br><br>
- <b>Model</b> -
    This is where we interact with database.
    <br><br>
    * <b>Handler</b> -
    This is responsible for manipulating results for a specific service / module. Focused on interacting / adjusting certain special tasks. Example Given: (ApiResultHandler)
    <br><br>
    * <b>Injectors</b> -
    This is responsible for setter type injection (Dependency Injection) of modules / services / repositories. For more info pls visit: (<a href="https://bearsunday.github.io/manuals/">https://bearsunday.github.io/manuals/1.0/en/di.html</a>).
    <br><br>
    * <b>Manager</b> -
    From the word itself. allows you to manage connections, application context, sessions. Usually as a central location where components throughout application can interact to.  Example Given: SessionManager, Auth Manager.
    <br><br>
    * <b>Repositories</b> -
    Repositories represent an architectural layer that handles communication between application and data source. This is where we interact with database.
<br><br>
- <b>Provider</b> -
    Central place of all Bear sunday application for bootstraping. What do we mean by bootstrap? In general, we mean registering things, whether its core framework or third party services.
<br><br>
- <b>Resource</b> -
    Acts as an entry point/s to your app RESTful resources.
    <br><br>
    * <b>App</b> -
    Resource class which allows you to interact with a model or repository.
    <br><br>
    * <b>Page</b> -
    Which is somehow has identical functionalities with a Controller.

###### <span style="color:#e6004c;">tests</span>

a location which consists of your application unit, feature tests.

###### <span style="color:#e6004c;">var</span>

- <b>conf</b> -
    contains router and constants for application
<br><br>
- <b>log</b> -
    contains application logs. it can be either informative or contains error logs
<br><br>
- <b>templates</b> -
    contains design for the application. Html layouts are usually structured here.
    <br><br>
    * <b>elements</b> -
    contains common page elements / parts.
    <br><br>
    * <b>errors</b> -
    contains application error page design
    <br><br>
    * <b>layouts</b> -
    contains application design layout.
    <br><br>
    * <b>pages</b> -
    contains individual page contents.
<br><br>
- <b>tmp</b> -
    contains temporary files produced by framework.
###### <span style="color:#e6004c;">vendor</span>

Contains third party and necessary dependencies to make framework functional and stay in optimal condition.