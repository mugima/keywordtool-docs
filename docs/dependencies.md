Echarts
======

<a href="http://echarts.baidu.com">
    ![Screenshot](img/logo.png)
</a>

<br>
ECharts is a free, powerful charting and visualization library offering an easy way of adding intuitive, interactive, and highly customizable charts to your commercial products. It is written in pure JavaScript and based on <a href="https://github.com/ecomfe/zrender">zrender</a>, which is a whole new lightweight canvas library.

<center><h4 style="color:red"><u>Mainly used in graph page</u></h4></center>

Google Charts
======
<a href="https://developers.google.com/chart/">
    ![](img/google.png)
</a>

<br>
Google chart tools are powerful, simple to use, and free.

<center><h4 style="color:red"><u>Mainly used in Timeline page</u></h4></center>

Koriyama Http Constants
======
<center><a href="https://github.com/koriym/Koriym.HttpConstants">https://github.com/koriym/Koriym.HttpConstants</a></center>


Contains the values of status codes, request method, and headers for the Hypertext Transfer Protocol (HTTP). Constants only.


Flatpickr
======
<center><a href="https://github.com/flatpickr/flatpickr">https://github.com/flatpickr/flatpickr</a></center>


Lightweight, powerful javascript datetimepicker with no dependencies


Papa Parse
======
<center><a href="https://github.com/mholt/PapaParse">https://github.com/mholt/PapaParse</a></center>


Fast and powerful CSV (delimited text) parser that gracefully handles large files and malformed input
