### Step 1: Ensure Google Charts dependency is loaded
```shell
npm install
npm run dev
```

### Step 2: Set chart options

Example Given: We used scatter plot chart for example

```javascript
    oOption = {
      title: 'ローリエプレス - ござる', // timeline title
      pointSize: 8, // bubble size
      height: 500, // height of the graph
      colors: ['red'], // line color
      series: {0: { lineDashStyle: [14, 2, 2, 7] }}, // line stye in between the bubbles
      selectionMode: 'multiple',
      tooltip: { // shown on mouse over
        trigger: 'highlight',
        isHtml: true
      },
      animation:{
        startup: true,
        duration: 600, // duration of the animation
        easing: 'in'
      },
      legend: 'none',
      hAxis: { // X Axis
        title: 'Tweets', // x axis label
        scaleType: 'linear', // scale type, either linear or log
      },
      vAxis: { // Y Axis
        title: 'Web Pages', // y axis label
        scaleType: 'linear',
      }
    };
```

### Step 3: Initialize Chart

```javascript
    google.charts.load('current', {packages: ['corechart']});
    google.charts.setOnLoadCallback(initChart); // initChart is the function that initiates the chart
```

Your chart will look like this if properly rendered:

![](img/timeline_log.png)

### Step 4: Changing Chart Value (Optional)
Handle event trigger as shown in the image below:
![](img/timeline_normal.png)

Code to handle event:
```javascript
    // Add Event Listener(Switching to Log or Normal values)
      if ($('input#influenceType').is(":checked")) {
        for (i = 0; i < oInfluenceSizes.length; i++) {
          // Manipulate xCoordinate values 
          if ($('#xCoordinates').is(":checked")) {
            oRawData[i][0] = parseFloat(aInfluencerValuesLog[i]); // values is changed to natural logarithm
          } else {
            oRawData[i][0] = parseFloat(aInfluencerValues[i]); // values is change to normal values
          }

          //Check yCoordinate State, apply conversion if needed 
          if ($('#yCoordinates').is(":checked")) {
            oRawData[i][1] = oBackUpRawData[i][1] > 0 ? parseFloat(Math.log(oBackUpRawData[i][1])) : 0;
          }

          // add the new points to the data array(bubble)
          data.addRow(oRawData[i]);
        }

        // redraw the chart
        oChart.draw(data, oAltered);
      }
```