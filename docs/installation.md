# Dev Environment Installation

### Step 1: Go to excite folder
```
cd ~/excite
```

### Step 2: Clone the project
```
git clone git@github.excite.ad.jp:Excite/Keywordtool.Front.git Keywordtool.Front\
```

#### - OR -
```
git clone https://github.excite.ad.jp/Excite/Keywordtool.Front Keywordtool.Front
```

### Step 3: Install necessary dependencies
```
composer install
```

### Step 4: Dev Settings (OPTIONAL)
```
composer setup-dev
```

### Step 5: Install Assets
```
npm install
npm run-dev
```