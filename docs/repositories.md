#### Step 1: Create Page Class

Create A Resource File
<center>src/Resource/Page/Sample.php</center>
```php
<?php
namespace Keyword\Analyzer\Resource\Page;

use BEAR\Resource\ResourceObject;
use BEAR\Resource\Annotation\Embed;

class Sample extends ResourceObject
{
    /**
     * Sample Page
     *
     *
     * @return ResourceObject
     */
    public function onGet() : ResourceObject
    {
        $this->body = [];

        return $this;
    }
}
```
<br>
#### Step 2: Create and Embed Resource
<center>src/Resource/Page/Sample.php</center>
```php
    /**
     * Sample Page
     *
     * @Embed(rel="alias", src="app://self/example")
     *
     * @return ResourceObject
     */
    public function onGet() : ResourceObject
    {
        $this->body = [
            'data' => $this['alias']->Body
        ];

        return $this;
    }
```

<center>src/Resource/App/Example.php</center>
```php
    use SampleRepositoryInject;

    /**
     * Some Description Here
     *
     *
     * @param string $name
     *
     * @return ResourceObject
     */
    public function onGet() : ResourceObject
    {
        // Create
        $this->body = $this->sampleRepository->getData();

        return $this;
    }
```
<br>
#### Step 3: Create Repository, Repository Data Source, and Repository Injector
<center>src/Model/Repositories/SampleRepository.php</center>
```php
class SampleRepository implements SampleInterface
{
    private $sampleDataSource;

    public function __construct(SampleDataSourceInterface $sampleDataSource)
    {
        $this->sampleDataSource = $sampleDataSource;
    }

    /**
     * Return All data
     *
     * return array
     */
    public function getData() : array
    {
        return $this->sampleDataSource->getAllData() ;
    }
}
```

<center>src/Model/Repositories/SampleRepositoryInterface.php</center>
```php
interface SampleRepositoryInterface
{
    /**
     * Return All data
     *
     * return array
     */
    public function getData() : array;
}
```

<center>src/Model/Repositories/SampleLocalDataSource.php</center>
```php
class SampleLocalDataSource implements SampleDataSource
{
    use AuraSqlInject;

    /**
     * Get All Data
     *
     *
     * @return array
     */
    public function getAllData() : array
    {
        $sql = '
            SELECT
              *
            FROM
              `table_name`
        ';

        $statement = $this->pdo->prepare($sql);
        $statement->execute();

        return $statement->fetchAll();
    }
}
```

<center>src/Model/Repositories/SampleDataSource.php</center>
```php
interface SampleDataSource
{
    /**
     * Get All Data
     *
     *
     * @return array
     */
    public function getAllData() : array;
}
```

<center>src/Model/Injectors/SampleRepositoryInject.php</center>
```php
trait SampleRepositoryInject
{
    /**
     * @var SampleRepositoryInterface
     */
    protected $sampleRepository;

    /**
     * @param SampleRepositoryInterface $sampleRepository
     *
     * @\Ray\Di\Di\Inject
     */
    public function setSampleRepository(SampleRepositoryInterface $sampleRepository)
    {
        $this->sampleRepository = $sampleRepository;
    }
}
```

<br>
#### Step 4: Bind Repository to RepositoryModule
<center>src/Module/RepositoryModule.php</center>

```php
use Keyword\Analyzer\Model\Repositories\{
    SampleRepository,
    SampleRepositoryInterface,
    SampleDataSource,
    SampleLocalDataSource
};

class RepositoryModule extends AbstractModule
{
    /**
     * Bindings
     */
    protected function configure()
    {
        $this->bind(SampleRepositoryInterface::class)->to(SampleRepository::class)->in(Scope::SINGLETON);
        $this->bind(SampleDataSource::class)->to(SampleLocalDataSource::class)->in(Scope::SINGLETON);
    }
}
```