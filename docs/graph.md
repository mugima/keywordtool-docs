### Step 1: Ensure Echarts dependency is loaded
```shell
npm install
npm run dev
```

### Step 2: Ensure proper data structure

According to Echarts, your data points / series data structure should look like this:

```javascript
    series: [{
        symbolSize: 20, // how big the dots will be
        data: [
            [10.0, 8.04] // First value is x-coordinate, Second value is y-coordinate
        ],
        type: 'scatter' // chart type
    }]
```

### Step 3: Set chart options

Example Given: We used scatter plot chart for example

![](img/points.png)

```javascript
    var chartOptions = {
        tooltip: {
            trigger: 'item'
        },
        xAxis: { // x-axis grid functionalities
            splitLine: { // grid style
                lineStyle: {
                    type: 'dashed'
                }
            },
            name: 'Tweets', // x-axis label name
            nameLocation: 'middle', // x-axis label position
            nameGap: 40, // x-axis label gap from chart
            type: 'value',
            min: 0 // x-axis minimum value
        },
        yAxis: { // y-axis grid functionalities
            splitLine: { // grid style
                lineStyle: {
                    type: 'dashed'
                }
            },
            type: 'value', // y-axis label name
            name: 'Web Pages', // y-axis label position
            nameLocation: 'middle', // y-axis label gap from chart
            nameGap: 40,
            min: 0 // y-axis minimum value
        },
        title: {
            top: 0, // top position
            text: {{ data[0].main_keyword|json_encode|raw }}, // chart title text
            subtext: 'From {{ data[0].term_start|raw }} to {{ data[0].term_end|raw }}', // chart title sub text
            left: 'center', // chart title position
            textStyle: { // chart title custom text styles
                color: '#000'
            }
        },
        series: {{ graph|json_encode|raw }}, // Given value in Step # 1
        toolbox: { // Used in datazoom - Echart Optional Toolbox
            show: false, // do we show toolbox?
            showTitle: true, // show title of toolbox
            orient: 'vertical', // toolbox orientation
            x: 'right', // x-coordinate toolbox placement
            y: 'bottom', // y-coordinate toolbox placement
            itemSize: 75, // how big should toolbox items be?
            padding: [0, 35, 60, 0], // padding alloted for each toolbox icon
        }
};
```

### Step 4: Initialize Chart

```javascript
    var chart = echarts.init(document.getElementById('main'));

    chart.setOption(chartOptions); // chartOptions is the value we created in Step # 3
```

Your chart will look like this if properly rendered:

![](img/graph.png)

### Step 5: Changing Chart Value (Optional)
Handle event trigger as shown in the image below:
![](img/trigger.png)

Code to handle event:
```javascript
    // Add Event Listener
    $('#aSampleTrigger').on('change', 'input[type="checkbox"]', function(e) {
        // To manipulate chart data
        $.each(chartOption.series, function(index, value) {
            // to manipulate x-values - X-Coordinate has value of 0
            chartOption.series[index].data[0][0] = 33; // Example manipulated x-coordinate value
            // to manipulate y-values - Y-Coordinate has value of 1
            chartOption.series[index].data[0][1] = 35; // Example manipulated y-coordinate value
        });

        // After manipulating value, Ensure to call chart instance so that changes will be reflected
        chart.setOption(chartOption);
    });
```